__author__ = 'lual'

import requests
from bs4 import BeautifulSoup  # easy_install beautifulsoup4

def movies_in_theaters():
    result = requests.get("http://www.imdb.com/movies-in-theaters/");
    soup = BeautifulSoup(result.text);

    groups = soup.find_all(name="div", attrs={"class": "list detail sub-list"});

    dic_group = dict();

    for group in groups:
        group_name = group.find(name="h3").text;
        movies = group.find_all(name="div", attrs={"itemtype": "http://schema.org/Movie"})

        group_actual = list();
        dic_group[group_name] = group_actual;

        for movie in movies:
            movie_name = movie.find_all(name="a", attrs={"itemprop": "url"})[0].text;
            movie_duration = movie.find_all(name="time", attrs={"itemprop": "duration"})[0].text;
            movie_genres = [item.text for item in movie.find_all(name="span", attrs={"itemprop": "genre"})];
            movie_cover = movie.find_all(name="img", attrs={"class": "poster shadowed"})[0]["src"];
            movie_description = movie.find_all(name="div", attrs={"itemprop": "description"})[0].text;
            movie_director = movie.find_all(name="span", attrs={"itemprop": "director"})[0];
            #movie_director_text = movie_director.find_all(name="span", attrs={"itemprop", "name"});

            dic_data = dict();
            dic_data["name"] = movie_name;
            dic_data["duration"] = movie_duration;
            dic_data["genres"] = ", ".join(movie_genres);
            dic_data["director"] = movie_director;
            dic_data["cover"] = movie_cover;
            dic_data["description"] = movie_description.strip();

            group_actual.append(dic_data)

    return dic_group;

if __name__ == '__main__':
    results = movies_in_theaters();

    for group_name, movies in results.items():
        print("\ngroup: {0}".format(group_name));
        for movie in movies:
            print("\tmovie: {0}".format(movie["name"]));
            print("\tduration: {0}".format(movie["duration"]));
            print("\tgenres: {0}".format(movie["genres"]));
            #print("\tdirector: {0}".format(movie["director"]));
            print("\timage_cover: {0}".format(movie["cover"]));
            print("\tdescription: {0}\n".format(movie["description"]));